package gravetics.in.otpapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by satish kadyan on 7/17/2017.
 */

public class SMSApiResultMessage implements Parcelable {

    @SerializedName("status")
    private int status;

    @SerializedName("to")
    private String to;

    @SerializedName("error-text")
    private String errorText;

    public SMSApiResultMessage(String errorText) {
        this.errorText = errorText;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getErrorText() {
        return errorText;
    }

    public void setErrorText(String errorText) {
        this.errorText = errorText;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeString(errorText);
        dest.writeString(to);
    }

    private SMSApiResultMessage(Parcel in) {
        this.status = in.readInt();
        this.errorText = in.readString();
        this.to = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<SMSApiResultMessage> CREATOR = new Parcelable.Creator<SMSApiResultMessage>() {

        @Override
        public SMSApiResultMessage createFromParcel(Parcel source) {
            return new SMSApiResultMessage(source);
        }

        @Override
        public SMSApiResultMessage[] newArray(int size) {
            return new SMSApiResultMessage[size];
        }
    };

    @Override
    public String toString() {
        return "SMSApiResultMessage{" +
                "status=" + status +
                ", to='" + to + '\'' +
                ", errorText='" + errorText + '\'' +
                '}';
    }
}

package gravetics.in.otpapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by satish kadyan on 7/17/2017.
 * mapping class for automatically mapping of json data returned from server call to Object representation by retrofit convertor factory
 */

public class ContactsResponse {
    @SerializedName("data")
    private List<Contact> data;

    public List<Contact> getResults() {
        return data;
    }

    public void setResults(List<Contact> data) {
        this.data = data;
    }

}

package gravetics.in.otpapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by satish kadyan on 7/17/2017.
 * json mapping of smsresponse from sms gateway api result , this mapping use to easy convert json string to java object easy way by retrofit
 */

public class SmsResponse {

    @SerializedName("message-count")
    private String messageCount;
    @SerializedName("messages")
    private List<SMSApiResultMessage> SMSApiResultMessages;

    public List<SMSApiResultMessage> getResults() {
        return SMSApiResultMessages;
    }

    public void setResults(List<SMSApiResultMessage> SMSApiResultMessages) {
        this.SMSApiResultMessages = SMSApiResultMessages;
    }

    public String getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(String messageCount) {
        this.messageCount = messageCount;
    }
}

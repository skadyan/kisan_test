package gravetics.in.otpapp.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import java.util.Date;

/**
 * Created by satish kadyan on 7/17/2017.
 * greendao POJO ORM object to represent mapping with sqlite table. annotation based mapping for easy and quick ORM mapping done by greendao
 */

@Entity(indexes = {
        @Index(value = "date DESC", unique = true)
})
public class SmsMessage {

    @Id
    private Long id;

    private String text;
    private String to;
    private String toName;
    private int otpCode;

    private Date date;

    @Generated(hash = 955085241)
    public SmsMessage(Long id, String text, String to, String toName, int otpCode,
                      Date date) {
        this.id = id;
        this.text = text;
        this.to = to;
        this.toName = toName;
        this.otpCode = otpCode;
        this.date = date;
    }

    @Generated(hash = 2118508277)
    public SmsMessage() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTo() {
        return this.to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public int getOtpCode() {
        return this.otpCode;
    }

    public void setOtpCode(int otpCode) {
        this.otpCode = otpCode;
    }

    @Override
    public String toString() {
        return "SmsMessage{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", to='" + to + '\'' +
                ", otpCode=" + otpCode +
                ", date=" + date +
                '}';
    }

    public String getToName() {
        return this.toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }
}

package gravetics.in.otpapp.model;

import android.content.Context;

import gravetics.in.otpapp.rest.APIClient;
import gravetics.in.otpapp.rest.ApiInterface;
import io.reactivex.Observable;

/**
 * Created by satish kadyan on 7/17/2017.
 * class to manage the contacts api calls
 */

public class ContactManager {
    private ApiInterface apiInterface;

    public ContactManager(Context context) {
        apiInterface = APIClient.getClient(context).create(ApiInterface.class);
    }

    //retreive contct list with rtrofit and rturn as obervable to caller
    public Observable<ContactsResponse> doGetContacts() {
        return apiInterface.doGetContacts();
    }
}

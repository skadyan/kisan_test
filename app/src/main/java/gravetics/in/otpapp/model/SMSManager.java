package gravetics.in.otpapp.model;

import android.content.Context;

import gravetics.in.otpapp.rest.ApiInterface;
import gravetics.in.otpapp.rest.SMSAPIClient;
import io.reactivex.Observable;

/**
 * Created by satish kadyan on 7/17/2017.
 * class to manage sms rest api request response handling
 */

public class SMSManager {
    private ApiInterface apiInterface;

    public SMSManager(Context context) {
        //rest interface for performing network rest api call to sms gateway server
        apiInterface = SMSAPIClient.getSMSAPIClient(context).create(ApiInterface.class);
    }

    // function to send request to post sms to a number with message detail and return data as obserable which can be observe on calling place(UI Thread)
    public Observable<SmsResponse> sendOTPToSmsGateway(String api_key, String api_secret, String to, String from, String text) {
        return apiInterface.sendOTPToSmsGateway(api_key, api_secret, to, from, text);
    }
}

package gravetics.in.otpapp.network;

import android.content.Context;

import java.io.IOException;

import gravetics.in.otpapp.common.NetworkUtils;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by satish kadyan on 7/17/2017.
 * interceptor implementation for okhttp3 to manage the network avalibility in easy way
 */

public class CustomNetworkInterceptor implements Interceptor {

    private final Context mContext;

    public CustomNetworkInterceptor(Context context) {
        this.mContext = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        // throw a custom exception if no internet connection is present to able to handle and display user messages
        if (!NetworkUtils.isOnline(mContext)) {
            throw new NoNetworkConnectionException();
        }
        Request.Builder request = chain.request().newBuilder();
        return chain.proceed(request.build());
    }
}


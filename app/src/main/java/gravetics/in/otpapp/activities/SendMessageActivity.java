package gravetics.in.otpapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gravetics.in.otpapp.MainApp;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.common.Utils;
import gravetics.in.otpapp.model.Contact;
import gravetics.in.otpapp.model.DaoSession;
import gravetics.in.otpapp.model.SMSApiResultMessage;
import gravetics.in.otpapp.model.SMSManager;
import gravetics.in.otpapp.model.SmsMessage;
import gravetics.in.otpapp.model.SmsMessageDao;
import gravetics.in.otpapp.model.SmsResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SendMessageActivity extends AppCompatActivity {

    @BindView(R.id.message_text)
    TextView otpMessageBox;
    @BindView(R.id.to_name_number_text)
    TextView toName;
    @BindView(R.id.main_content)
    View mainContent;

    private Contact contact;
    private CompositeDisposable mCompositeDisposable;
    private int otpCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        ButterKnife.bind(this);

        //get the parcelable contact detail from contact detail activity
        contact = getIntent().getParcelableExtra(AppConstants.CONTACT_DATA);

        //setup material toolbar, instead of older actionbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeButtonEnabled(true);
        ab.setTitle(getString(R.string.activity_sendmessage_title));
        toName.setText(contact.getFirstName() + " " + contact.getLastName() + " (" + contact.getNumber() + ")");
        otpCode = Utils.generateOtp();
        otpMessageBox.setText(String.format(getString(R.string.otp_message_format), otpCode));
        mCompositeDisposable = new CompositeDisposable();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        // handle on back/up button click in toolbar to move to previous activity ie contact detail activity
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    //send sms button click handler
    @OnClick(R.id.send_message)
    public void sendOtpClick(View v) {
        v.setEnabled(false);

        SMSManager smsManager = new SMSManager(this.getApplicationContext());

        // async sms gateway send OTP sms to requested contact

        mCompositeDisposable.add(smsManager.sendOTPToSmsGateway(AppConstants.SMS_API_KEY, AppConstants.SMS_API_SERCET, contact.getNumber(), AppConstants.MY_NUMBER, otpMessageBox.getText().toString()).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<SmsResponse>() {

                    @Override
                    public void onNext(@NonNull SmsResponse smsResponse) {
                        //async response from rxjava sms gateway result handling
                        handleResponse(smsResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                }));

    }

    // display error to user if there is some problem with server call for contactlist data retrieval
    private void handleError(Throwable error) {
        Snackbar errorSnackbar = Snackbar.make(mainContent, R.string.no_internet_title, Snackbar.LENGTH_LONG);
        errorSnackbar.show();
    }

    // sms gateway response handler to handle response returned from sms gateway
    private void handleResponse(SmsResponse smsResponse) {
        ArrayList<SMSApiResultMessage> SMSApiResultMessages = (ArrayList<SMSApiResultMessage>) smsResponse.getResults();
        if (SMSApiResultMessages != null) {
            SMSApiResultMessage SMSApiResultMessage = SMSApiResultMessages.get(0);

            // sms sent successfully, now sms data can be saved to db, so that can display in sms sent list
            if (SMSApiResultMessage.getStatus() == 0) {
                SMSApiResultMessage.setErrorText(getString(R.string.success_otp_sent));
                SmsMessage smsMessage = new SmsMessage();
                smsMessage.setText(otpMessageBox.getText().toString());
                smsMessage.setOtpCode(otpCode);
                smsMessage.setTo(contact.getNumber());
                smsMessage.setToName(Utils.nameFormat(contact.getFirstName(), contact.getLastName()));
                smsMessage.setDate(new Date());
                DaoSession daoSession = ((MainApp) getApplication()).getDaoSession();
                SmsMessageDao smsMessageDao = daoSession.getSmsMessageDao();
                smsMessageDao.insert(smsMessage);
            }

            Intent intent = new Intent();
            intent.putExtra(AppConstants.SMS_API_RESULT, SMSApiResultMessage);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            SMSApiResultMessage SMSApiResultMessage = new SMSApiResultMessage(getString(R.string.unknown_error));
            Intent intent = new Intent();
            intent.putExtra(AppConstants.SMS_API_RESULT, SMSApiResultMessage);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}

package gravetics.in.otpapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.model.Contact;
import gravetics.in.otpapp.model.SMSApiResultMessage;

public class ContactDetailActivity extends AppCompatActivity {

    private Contact contact;
    @BindView(R.id.first_name)
    TextView firstname;
    @BindView(R.id.last_name)
    TextView lastname;
    @BindView(R.id.number)
    TextView number;
    @BindView(R.id.message_card)
    CardView messageCard;
    @BindView(R.id.message_card_text)
    TextView messageCardText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);

        //butterknife bind view call to bind views to fields with annotation, get rid of calling and casting findviewbyid repeatly
        ButterKnife.bind(this);

        //setup material toolbar, instead of old fashion actionbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeButtonEnabled(true);
        ab.setTitle(getString(R.string.activity_contactdetail_title));

        // get contact detail data from intent passed from preious activity
        contact = getIntent().getParcelableExtra(AppConstants.CONTACT_DATA);

        firstname.setText(contact.getFirstName());
        lastname.setText(contact.getLastName());
        number.setText(contact.getNumber());
    }

    //onclick handler for send message button, binding provided by butterknife with annotation
    @OnClick(R.id.send_message)
    public void sendSMSClick(View v) {
        Intent intent = new Intent(this, SendMessageActivity.class);
        intent.putExtra(AppConstants.CONTACT_DATA, contact);
        startActivityForResult(intent, AppConstants.SEND_OTP_ACTIVITY_RESULT_CODE);
    }

    //handle the result from send OTP SMS activity to display user friendly messages about, failure or success of sms gateway send otp action
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == AppConstants.SEND_OTP_ACTIVITY_RESULT_CODE) {
            SMSApiResultMessage SMSApiResultMessage = data.getParcelableExtra(AppConstants.SMS_API_RESULT);
            if (SMSApiResultMessage != null) {
                messageCardText.setText(SMSApiResultMessage.getErrorText());
                messageCard.setVisibility(View.VISIBLE);
            }
        }
    }
}
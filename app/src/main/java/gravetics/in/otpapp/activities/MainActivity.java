package gravetics.in.otpapp.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.adapters.ViewPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = MainActivity.class.getName();
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //butterknife bind view call to bind views to fields with annotation, get rid of calling and casting findbyview repeatly
        ButterKnife.bind(this);
        initViews();
    }

    private void initViews() {
        //set material toolbar, instead of old fashioned actionbar
        setSupportActionBar(myToolbar);
        //setup viewpager to setup tabs for display contact list and sms sent list fragment as viewpager and tabs
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}

package gravetics.in.otpapp.rest;

import gravetics.in.otpapp.model.ContactsResponse;
import gravetics.in.otpapp.model.SmsResponse;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by satish kadyan on 7/17/2017.
 * interface used by retrofit to perform rest api in easy way
 */

public interface ApiInterface {

    // return contact list from server
    @GET("contacts")
    Observable<ContactsResponse> doGetContacts();

    // send a OTP to sms gateway and return result to caller manager class
    @POST("sms/json")
    @FormUrlEncoded
    Observable<SmsResponse> sendOTPToSmsGateway(@Field("api_key") String api_key, @Field("api_secret") String api_secret, @Field("to") String to, @Field("from") String from, @Field("text") String text);


}

package gravetics.in.otpapp.rest;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.network.CustomNetworkInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by satish kadyan on 7/17/2017.
 * retrofit instance for sms gateway
 */

public class SMSAPIClient {
    private static Retrofit mRetrofit;

    // return a retrofit instance to perform sms gateway api call
    public static Retrofit getSMSAPIClient(Context context) {
        CustomNetworkInterceptor customNetworkInterceptor = new CustomNetworkInterceptor(context);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).addInterceptor(customNetworkInterceptor).build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.SMS_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        return mRetrofit;
    }

}

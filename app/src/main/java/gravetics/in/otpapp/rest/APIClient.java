package gravetics.in.otpapp.rest;

import android.content.Context;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.network.CustomNetworkInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by satish kadyan on 7/17/2017.
 * provide a retrofit instance to perform network requests
 */

public class APIClient {
    private static Retrofit mRetrofit;

    // prepare and return retrofit instance
    public static Retrofit getClient(Context context) {
        CustomNetworkInterceptor customNetworkInterceptor = new CustomNetworkInterceptor(context);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(customNetworkInterceptor).build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        return mRetrofit;
    }

}

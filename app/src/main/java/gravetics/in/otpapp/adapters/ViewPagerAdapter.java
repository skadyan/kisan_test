package gravetics.in.otpapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.fragments.ContactListFragment;
import gravetics.in.otpapp.fragments.SMSListFragment;


/**
 * Created by satish kadyan on 7/17/2017.
 * viewpager adapter to handle tabs/pager on home activity to display contact list and sent sms list fragments
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        //display contact list fragment on 1st tab
        if (position == 0) {
            fragment = new ContactListFragment();
        }

        //display csent sms list fragment on 2nd tab
        else if (position == 1) {
            fragment = new SMSListFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;

        // set title of tab for 1st tab
        if (position == 0) {
            title = AppConstants.TAB_TITLE_0;
        }

        // set title of tab for 2nd tab
        else if (position == 1) {
            title = AppConstants.TAB_TITLE_1;
        }
        return title;
    }
}

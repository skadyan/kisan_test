package gravetics.in.otpapp.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import gravetics.in.otpapp.R;
import gravetics.in.otpapp.common.Utils;
import gravetics.in.otpapp.model.Contact;

/**
 * Created by satish kadyan on 7/17/2017.
 */

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private final Activity activity;
    private ArrayList<Contact> contactList;

    public ContactsAdapter(ArrayList<Contact> contactList, Activity activity) {
        this.contactList = contactList;
        this.activity = activity;
    }

    @Override
    public ContactsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_contact, parent, false);
        return new ContactsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactsAdapter.ViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        holder.rootItem.setTag(contact);
        holder.name.setText(Utils.nameFormat(contact.getFirstName(), contact.getLastName()));
        if (contact.getNumber() != null) {
            holder.number.setText(contact.getNumber());
        }
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    // updating data of adapter and refresh the list to update list display new data
    public void setResults(ArrayList<Contact> contacts) {
        contactList = contacts;
        notifyDataSetChanged();
    }

    // reusable viewholder pattern for better reusing views rows in listview
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView number;
        private LinearLayout rootItem;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            number = (TextView) itemView.findViewById(R.id.number);
            rootItem = (LinearLayout) itemView.findViewById(R.id.root_item);
        }
    }
}

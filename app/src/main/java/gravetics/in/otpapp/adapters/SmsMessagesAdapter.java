package gravetics.in.otpapp.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import gravetics.in.otpapp.R;
import gravetics.in.otpapp.common.Utils;
import gravetics.in.otpapp.model.SmsMessage;

/**
 * Created by satish kadyan on 7/17/2017.
 * adapter to display list of  sms message sent for otp
 */

public class SmsMessagesAdapter extends RecyclerView.Adapter<SmsMessagesAdapter.ViewHolder> {

    private final Activity activity;
    private ArrayList<SmsMessage> smsMessageList = new ArrayList<>();


    public SmsMessagesAdapter(List<SmsMessage> SmsList, Activity activity) {
        this.smsMessageList = (ArrayList<SmsMessage>) SmsList;
        this.activity = activity;
    }

    @Override
    public SmsMessagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_sms, parent, false);
        return new SmsMessagesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SmsMessagesAdapter.ViewHolder holder, int position) {

        // prepare the view populated to display in recyclewview list
        SmsMessage smsMessage = smsMessageList.get(position);
        holder.rootItem.setTag(smsMessage);
        if (smsMessage.getTo() != null) {
            holder.toNumber.setText(smsMessage.getTo());
        }
        if (smsMessage.getTo() != null) {
            holder.toName.setText(smsMessage.getToName());
        }
        holder.otp.setText(String.format(activity.getString(R.string.otp_text_format), smsMessage.getOtpCode()));
        if (smsMessage.getDate() != null) {
            String formatedDate = Utils.dateFormat(smsMessage.getDate());
            holder.date.setText(formatedDate);
        }
    }

    @Override
    public int getItemCount() {
        return smsMessageList.size();
    }

    // update data with new dataset and refresh listview to reflect new data
    public void setResults(ArrayList<SmsMessage> messages) {
        smsMessageList = messages;
        notifyDataSetChanged();
    }

    // reusable viewholder pattern to better and effective managing reusable items used in listview with scroll
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView toName;
        private TextView toNumber;
        private TextView otp;
        private TextView date;
        private LinearLayout rootItem;

        public ViewHolder(View itemView) {
            super(itemView);
            toName = (TextView) itemView.findViewById(R.id.to_name);
            toNumber = (TextView) itemView.findViewById(R.id.to_number);
            otp = (TextView) itemView.findViewById(R.id.otp);
            date = (TextView) itemView.findViewById(R.id.date);
            rootItem = (LinearLayout) itemView.findViewById(R.id.root_item);
        }
    }
}

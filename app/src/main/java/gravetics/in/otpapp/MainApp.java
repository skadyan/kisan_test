package gravetics.in.otpapp;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import gravetics.in.otpapp.model.DaoMaster;
import gravetics.in.otpapp.model.DaoSession;

/**
 * Created by satish kadyan on 7/17/2017.
 * custom Application class used by application, here you can perform application level operation
 */

public class MainApp extends Application {
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        // create and provide greendao session object once when application launch, here initiaze make it available throughout application
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "otpapp-db");
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
    }

    // return database session to perform sqlite operation provided by greendao
    public DaoSession getDaoSession() {
        return daoSession;
    }
}

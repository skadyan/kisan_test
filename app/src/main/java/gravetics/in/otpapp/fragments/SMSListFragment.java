package gravetics.in.otpapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.greendao.query.Query;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gravetics.in.otpapp.MainApp;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.adapters.SmsMessagesAdapter;
import gravetics.in.otpapp.model.DaoSession;
import gravetics.in.otpapp.model.SmsMessage;
import gravetics.in.otpapp.model.SmsMessageDao;
import io.reactivex.annotations.Nullable;

/**
 * Created by satish kadyan on 7/17/2017.
 * fragment to display sent sms saved in sqlite database
 */

public class SMSListFragment extends Fragment {

    private final static String TAG = SMSListFragment.class.getName();

    private SmsMessagesAdapter adapter;
    @BindView(R.id.empty_box)
    TextView emptyView;
    @BindView(R.id.card_recycler_view)
    RecyclerView mRecyclerView;
    private Query<SmsMessage> messagesQuery;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_smslist, container, false);

        // bind views in fagment with butterknife for easy access and avoid findbyview calls
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void loadSmsMessages() {

        // retreive singelton greendao session object, intialized in application class to perform database operations
        DaoSession daoSession = ((MainApp) getActivity().getApplication()).getDaoSession();

        // retrieve greendao smsmessage dao to which can be used to get db operation on smsmessage table.
        SmsMessageDao smsMessageDao = daoSession.getSmsMessageDao();

        // retreive all the list of smsmessages from smsmessage table in desc date ordering
        messagesQuery = smsMessageDao.queryBuilder().orderDesc(SmsMessageDao.Properties.Date).build();
        List<SmsMessage> messageList = messagesQuery.list();

        // if sms messages are empty then display empty text and hide list
        if (messageList.size() > 0) {
            emptyView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            mRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            mRecyclerView.setLayoutManager(layoutManager);
            adapter = new SmsMessagesAdapter(messageList, getActivity());
            mRecyclerView.setAdapter(adapter);
            DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL
            );
            mRecyclerView.addItemDecoration(mDividerItemDecoration);

        } else {
            mRecyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        //load the sms sent list from sqlite
        loadSmsMessages();

    }

}

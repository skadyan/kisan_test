package gravetics.in.otpapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import gravetics.in.otpapp.R;
import gravetics.in.otpapp.activities.ContactDetailActivity;
import gravetics.in.otpapp.adapters.ContactsAdapter;
import gravetics.in.otpapp.common.AppConstants;
import gravetics.in.otpapp.listeners.ContactClickListener;
import gravetics.in.otpapp.model.Contact;
import gravetics.in.otpapp.model.ContactManager;
import gravetics.in.otpapp.model.ContactsResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.annotations.Nullable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by satish kadyan on 7/17/2017.
 * fragment to display list of contacts
 */

public class ContactListFragment extends Fragment implements ContactClickListener.OnItemClickListener {

    private final static String TAG = ContactListFragment.class.getName();

    private ContactsAdapter adapter;
    @BindView(R.id.error_text)
    TextView emptyView;
    @BindView(R.id.progressView)
    ProgressBar mProgressView;
    @BindView(R.id.card_recycler_view)
    RecyclerView mRecyclerView;
    private CompositeDisposable mCompositeDisposable;
    private Snackbar errorSnackbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contactlist, container, false);

        //bind view to variable annotated with butterknife
        ButterKnife.bind(this, view);

        //RXjava disposable to handle async call to get list of contact on other thread, and display back them in listview on main/UI thread
        mCompositeDisposable = new CompositeDisposable();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(layoutManager);
        ArrayList contacts = new ArrayList<>();
        adapter = new ContactsAdapter(contacts, getActivity());
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.addOnItemTouchListener(new ContactClickListener(getActivity().getApplicationContext(), this));
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL
        );
        mRecyclerView.addItemDecoration(mDividerItemDecoration);

        //loading of contacts list from server call
        loadContacts();
    }

    private void loadContacts() {
        emptyView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mProgressView.setVisibility(View.VISIBLE);

        ContactManager contactManager = new ContactManager(getActivity().getApplicationContext());

        // RXjava disposable call and add subscription on result from contact list api
        mCompositeDisposable.add(contactManager.doGetContacts().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(new DisposableObserver<ContactsResponse>() {

                    // on next item emitted from observable handling
                    @Override
                    public void onNext(@NonNull ContactsResponse contactsResponse) {
                        handleResponse(contactsResponse);
                    }

                    // on error handling of observable
                    @Override
                    public void onError(Throwable e) {
                        handleError(e);
                    }

                    // on complete call of observable when it is done with its task fully
                    @Override
                    public void onComplete() {

                    }
                }));
    }

    //handler for onclick contact item in recycle view item,and open contact detail activity
    @Override
    public void itemClick(View v, int position) {
        Intent intent = new Intent(getActivity(), ContactDetailActivity.class);
        intent.putExtra(AppConstants.CONTACT_DATA, (Parcelable) v.getTag());
        startActivity(intent);
    }

    // handler for contacts list returned from Rxjava disposable to handle data returned from server with contact list
    private void handleResponse(ContactsResponse contactList) {
        adapter.setResults((ArrayList<Contact>) contactList.getResults());
        mProgressView.setVisibility(View.GONE);
        emptyView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);

    }

    // display error to user if there is some problem with server call for contactlist data retrieval
    private void handleError(Throwable error) {
        mProgressView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        errorSnackbar = Snackbar.make(mRecyclerView, R.string.no_internet_title, Snackbar.LENGTH_INDEFINITE);
        errorSnackbar.setAction(R.string.try_again, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadContacts();
            }
        });
        errorSnackbar.show();
    }
}

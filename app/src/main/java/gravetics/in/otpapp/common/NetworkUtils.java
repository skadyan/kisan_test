package gravetics.in.otpapp.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by satish kadyan on 7/17/2017.
 * network util to check and return state of network related operation
 */

public class NetworkUtils {

    // function to check if internet connection if available or not , mobile or wifi data connection state checking
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }
}

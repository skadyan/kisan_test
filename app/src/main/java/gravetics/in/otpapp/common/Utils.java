package gravetics.in.otpapp.common;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by satish kadyan on 7/17/2017.
 * util class for application for providing common reusable quick function in application
 */

public class Utils {

    // generate 6 digit random otp to be used for sending with sms
    public static int generateOtp() {
        Random rnd = new Random();
        int n = 100000 + rnd.nextInt(900000);
        return n;
    }

    // format the date to disred format to display in application
    public static String dateFormat(Date date) {
        SimpleDateFormat dt1 = new SimpleDateFormat("dd MMMM");
        String formatedDate = dt1.format(date);
        return formatedDate;
    }

    // format the name from firstname last name combination in unified way at all places in application
    public static String nameFormat(String firstName, String lastName) {
        String nameFormatted = "";
        if (firstName != null && firstName.length() > 0) {
            nameFormatted = firstName;
        }
        if (lastName != null && lastName.length() > 0) {
            if (nameFormatted.length() > 0) {
                nameFormatted = nameFormatted + " " + lastName;

            } else {
                nameFormatted = lastName;

            }
        }
        return nameFormatted;
    }

}

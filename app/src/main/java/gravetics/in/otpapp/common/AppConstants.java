package gravetics.in.otpapp.common;

/**
 * Created by satish kadyan on 7/17/2017.
 * common place to manage static and constant variables for aplication
 */

public class AppConstants {

    public static final String TAB_TITLE_0 = "Contact List";
    public static final String TAB_TITLE_1 = "OTP Sents";
    public static final String BASE_URL = "http://api.kedartravel.com/";
    public static final String SMS_API_BASE_URL = "https://rest.nexmo.com/";
    public static final String CONTACT_DATA = "contact_data";
    public static final String MY_NUMBER = "918901005351";
    public static final String SMS_API_KEY = "b0d03a3b";
    //public static final String SMS_API_SERCET = "eb6dc6d2b5941a48"; //wrong
    public static final String SMS_API_SERCET = "eb6dc6d2b5941a4d"; // correct
    public static final String SMS_API_RESULT = "sms_api_response_data";
    public static final int SEND_OTP_ACTIVITY_RESULT_CODE = 2;

}

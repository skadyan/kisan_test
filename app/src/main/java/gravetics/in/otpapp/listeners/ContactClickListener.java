package gravetics.in.otpapp.listeners;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by satish kadyan on 7/17/2017.
 * onclick listener handler for contact item
 */

public class ContactClickListener implements RecyclerView.OnItemTouchListener {
    final static String TAG = ContactClickListener.class.getSimpleName();
    GestureDetector mGestureDetector;

    OnItemClickListener mListener;

    public interface OnItemClickListener {
        public void itemClick(View v, int position);
    }

    public ContactClickListener(Context context, OnItemClickListener mListener) {
        this.mListener = mListener;
        this.mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mGestureDetector.onTouchEvent(e)) {
            if (this.mListener != null) {

                // callback view elements to listener attached
                mListener.itemClick(childView, rv.getChildAdapterPosition(childView));
            }
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
